import { Router } from "express"
import dao from "./dao"

const router = Router()

router.get("/:username", async (req, res) => {
    const username = req.params.username
    console.log(username)
    const result = await dao.getComments(username)
    res.send(result.rows)
})

router.post("/:id", async (req, res) => {
    console.log("DELETE")
    const id = Number(req.params.id)
    const result = await dao.deleteComment(id)
    res.sendStatus(200)    
})

export default router