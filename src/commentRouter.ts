import { Router } from "express"
import dao from "./dao"

const router = Router()

router.post("/", async (req, res) =>{
    const user = req.body
    const result = await dao.insertComment(user)
    const storedUser = {id: result.rows[0], ...user}
    res.send(storedUser)
})

router.post("/update/:id",async (req, res) => {
    console.log("UPDATE")
    const id = Number(req.params.id)
    const content = req.body.content
    const result = await dao.updateComment(id, content)
    res.sendStatus(200)
})

export default router