import server from "./server"

const PORT = Number(process.env.PORT)
server.listen(PORT, () => {
    console.log('Forum API listening to port', PORT)
})