import {pool} from "../src/db"
import { jest } from '@jest/globals'
import request from 'supertest'
import server from "../src/server"


describe("Dummy test", () => {
    test('Returns true', () => {
      expect(true).toBe(true)
    })

})

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /users', () => {
    const mockResponse = {
        rows: [
            { id: 1, username: "user1", full_name: 'user one', email: "user1@gmail.com" },
            { id: 2, username: "user2", full_name: 'user two', email: "user2@gmail.com" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    //Get all users
    test('Get all users', async () => {
        const response = await request(server).get('/users')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    //Get user
    test("Get one user", async () => {
        const response = await request(server).get("/users/1")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    //Insert user
    test("Insert new user", async () => {
        const response = await request(server).post("/users").query({username: "user3", full_name: 'user 3', email: "user3@gmail.com", id: 3})
        expect(response.statusCode).toBe(200)
        expect(response.body.id).toStrictEqual(mockResponse.rows[0])
    })

    //Delete user
    test("Delete user", async () => {
        const response = await request(server).post("/users/user1")
        expect(response.statusCode).toBe(200)
    })

    //Update user
    test("Update user", async () => {
        const response = await request(server).post("/users/update/user1")
        expect(response.statusCode).toBe(200)
    })
})


