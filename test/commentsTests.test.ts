import {pool} from "../src/db"
import { jest } from '@jest/globals'
import request from 'supertest'
import server from "../src/server"


describe("Dummy test", () => {
    test('Returns true', () => {
      expect(true).toBe(true)
    })

})

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /comments', () => {
    const mockResponse = {
        rows: [
            { commentid: 1, username: "user1", content: "first comment yay", comment_date: "2024-01-30", postid: "1" },
            { commentid: 2, username: "user2", content: "second comment mmmm", comment_date: "2024-01-30", postid: "1" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    //Get comment
    test("Get one comment", async () => {
        const response = await request(server).get("/comments/user1")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })


    //Insert comment
    test("Insert new comment", async () => {
        const response = await request(server).post("/comment").query({commentname: "comment3", full_name: 'comment 3', email: "comment3@gmail.com", id: 3})
        expect(response.statusCode).toBe(200)
        expect(response.body.id).toStrictEqual(mockResponse.rows[0])
    })

    //Delete comment
    test("Delete comment", async () => {
        const response = await request(server).post("/comments/1")
        expect(response.statusCode).toBe(200)
    })

    //Update comment
    test("Update comment", async () => {
        const response = await request(server).post("/comment/update/1")
        expect(response.statusCode).toBe(200)
    })
})


