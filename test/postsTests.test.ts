import {pool} from "../src/db"
import { jest } from '@jest/globals'
import request from 'supertest'
import server from "../src/server"


describe("Dummy test", () => {
    test('Returns true', () => {
      expect(true).toBe(true)
    })

})

const initializeMockPool = (mockResponse: any) => {
    (pool as any).connect = jest.fn(() => {
        return {
            query: () => mockResponse,
            release: () => null
        }
    })
}

describe('Testing GET /posts', () => {
    const mockResponse = {
        rows: [
            { postid: 1, comments: "[]", title: 'post one', content: "First post yay", post_date: "2024-01-30", username: "user1" },
            { postid: 2, comments: "[]", title: 'post two', content: "Second post mmmm", post_date: "2024-01-30", username: "user2" }
        ]
    }

    beforeAll(() => {
        initializeMockPool(mockResponse)
    })

    afterAll(() => {
        jest.clearAllMocks()
    })

    //Get all posts
    test('Get all posts', async () => {
        const response = await request(server).get('/posts')
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    //Get post
    test("Get one post", async () => {
        const response = await request(server).get("/posts/1")
        expect(response.statusCode).toBe(200)
        expect(response.body).toStrictEqual(mockResponse.rows)
    })

    //Insert post
    test("Insert new post", async () => {
        const response = await request(server).post("/posts").query({postname: "post3", full_name: 'post 3', email: "post3@gmail.com", id: 3})
        expect(response.statusCode).toBe(200)
        expect(response.body.id).toStrictEqual(mockResponse.rows[0])
    })

    //Delete post
    test("Delete post", async () => {
        const response = await request(server).post("/posts/1")
        expect(response.statusCode).toBe(200)
    })

    //Update post
    test("Update post", async () => {
        const response = await request(server).post("/posts/update/1")
        expect(response.statusCode).toBe(200)
    })
})


